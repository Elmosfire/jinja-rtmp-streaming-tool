# jinja-rtmp-streaming-tool

A small package to produce visualisations for streaming AI's and other automated systems. 

Just create a jinja2 template, and a generator that produces the data that needs to be filled in it, and this package will stream the resulting changing html page to rtmp or a video file.

## Getting started

needs ffmpeg to be installed and be in path

To install use

```
pip install git+https://gitlab.com/Elmosfire/jinja-rtmp-streaming-tool.git
```

To use call the function 

```python
from streamingtool import stream
stream("path/to/template", generator, size, url)
```

`path/to/template` is a path to a jinja2 template

`generator` is a generator of dictionaries. Each frame, a dictionary pops from the generator and is used to render the jinja2-template with.

`size` is the width and height in pixels.

`url` is the url of the rtmp server. for twitch for example this is `rtmp://iad05.contribute.live-video.net/app/{stream_key}`
You can also put a filename to save it to a video instead.
If ffmpeg stupports the output, this program will too. 
