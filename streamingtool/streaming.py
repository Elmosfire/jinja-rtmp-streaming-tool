from jinja2 import Environment, FileSystemLoader
from html2image import Html2Image
from PIL import Image
import subprocess as sp
import numpy as np
from time import time

# Generating all of the screenshots as base64 
# in a variable called screenshot_list



def stream(template, generator_, size, url, fps=30, live=True):
    w, h = size
    starttime = time()
    frames = 0
    def behind():
        return starttime + frames/fps < time()
    if live:
        cmd_out = [
            'ffmpeg',
            '-f', 'rawvideo',
            '-vcodec','rawvideo',
            '-s', f'{w}x{h}',
            '-pix_fmt', 'rgb24',
            '-re',
            '-i', '-',
            '-f', 'flv',
            '-vcodec', 'libx264',
            '-profile:v', 'main',
            '-g', '60',
            '-keyint_min', '30',
            '-pix_fmt', 'yuv420p',
            '-preset', 'ultrafast',
            '-tune', 'zerolatency',
            url,
            ]
    else:
        cmd_out = [
            'ffmpeg',
            '-f', 'rawvideo',
            '-vcodec','rawvideo',
            '-s', f'{w}x{h}',
            '-pix_fmt', 'rgb24',
            '-r', f"{fps}",
            '-i', '-',
            '-f', 'flv',
            '-vcodec', 'libx264',
            '-profile:v', 'main',
            '-g', '60',
            '-keyint_min', '30',
            '-pix_fmt', 'yuv420p',
            '-preset', 'ultrafast',
            '-tune', 'zerolatency',
            url,
            ]

    pipe = sp.Popen(cmd_out, stdin=sp.PIPE)

    hti = Html2Image().screenshot

    env = Environment(
        loader=FileSystemLoader('./'),
    )
    if template is not None:
        template_core = env.get_template(template)
    else:
        template_core = None
    try:
        for x in generator_:
            html_str = template_core.render(**x) if template_core is not None else x
            with open("tmp.html", "w") as file:
                file.write(html_str)
            hti(html_str=html_str, save_as='tmp.png', size=(w, h))
            im = Image.open('tmp.png').resize((w, h))
            crs = np.array(im)[:,:,:3].tobytes()
            if live:
                while behind():
                    frames += 1
                    pipe.stdin.write(crs)
            else:
                pipe.stdin.write(crs)
        
    finally:
        pipe.stdin.close()
        pipe.wait()

        # Make sure all went well
        if pipe.returncode != 0:
            raise sp.CalledProcessError(pipe.returncode, cmd_out)
        
def stream_direct(generator_, size, url, fps=30):
    yield from stream(None, generator_, size, url, fps=fps)