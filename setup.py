from setuptools import setup

with open("version") as file:
    version = ''.join(file).strip()

setup(
    name="streamingtool",
    version=version,
    description="A small package to produce visualisations for streaming AI's and other automated systems.",
    url="https://gitlab.com/Elmosfire/jinja-rtmp-streaming-tool/-/tree/main",
    author="Thorvald Dox",
    author_email="thorvalddx94@gmail.com",
    license="GPL3",
    packages=["streamingtool"],
    install_requires=["jinja2", "html2image@git+https://github.com/thorvalddox/html2image.git","Pillow", "numpy"],
    classifiers=[],
)
